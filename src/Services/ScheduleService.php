<?php
/**
 * Created by PhpStorm.
 * User: gedeo
 * Date: 11-Apr-21
 * Time: 13:38
 */

namespace App\Services;


use App\Entity\ScheduleClass;
use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\User\UserInterface;

class ScheduleService
{

    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function AddScheduleDates(ScheduleClass $scheduleClass, UserInterface $user)
    {
        $checkTime=$this->CheckTimeIsFree($scheduleClass);
        if($checkTime){
            return ['error'=>"This Time is already token."];
        }
        $scheduleClass->setCreatedBy($user);
        $scheduleClass->setCreatedDate(new \Datetime());
        $scheduleClass->setUpdatedDate(new \DateTime());
        $entityManager = $this->em;
        $entityManager->persist($scheduleClass);
        $entityManager->flush();

        return ["success"];
    }
    public function EditScheduleDates(ScheduleClass $scheduleClass, UserInterface $user)
    {
        $checkTime=$this->CheckTimeIsFree($scheduleClass);
        if($checkTime){
            return ['error'=>"This Time is already token."];
        }

        $entityManager = $this->em;
        $entityManager->flush();

        return ["success"];
    }

    public function CheckTimeIsFree(ScheduleClass $scheduleClass)
    {
        $em = $this->em->getConnection()->prepare(
            'SELECT * FROM `schedule_class` 
            WHERE ((:start_time<start_time AND :end_time>end_time) OR (end_time>:start_time AND :start_time>start_time) 
            OR (end_time>:end_time AND :end_time>start_time)) AND WEEKDAY=:weekday
            AND teacher_id=:teacher_id'
        );
        $em->bindValue('start_time', $scheduleClass->getStartTime()->format("H:i:s"));
        $em->bindValue('end_time', $scheduleClass->getEndTime()->format("H:i:s"));
        $em->bindValue('weekday', $scheduleClass->getWeekday());
        $em->bindValue('teacher_id', $scheduleClass->getTeacher()->getId());
        $em->execute();
        return $em->fetchAllAssociative();
    }
}