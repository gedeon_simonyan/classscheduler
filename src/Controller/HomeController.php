<?php
/**
 * Created by PhpStorm.
 * User: gedeo
 * Date: 20-Mar-21
 * Time: 23:17
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{

    public function index(){
        return $this->render('home.html.twig');
    }
}