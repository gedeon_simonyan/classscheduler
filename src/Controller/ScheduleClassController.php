<?php

namespace App\Controller;

use App\Entity\ScheduleClass;
use App\Form\ScheduleClassType;
use App\Services\ScheduleService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/schedule/class")
 */
class ScheduleClassController extends AbstractController
{
    /**
     * @Route("/", name="schedule_class_index", methods={"GET"})
     */
    public function index(): Response
    {
        $scheduleClasses = $this->getDoctrine()
            ->getRepository(ScheduleClass::class)
            ->findAll();

        return $this->render('schedule_class/index.html.twig', [
            'schedule_classes' => $scheduleClasses,
        ]);
    }

    /**
     * @Route("/new", name="schedule_class_new", methods={"GET","POST"})
     */
    public function new(Request $request, ScheduleService $scheduleService): Response
    {
        $scheduleClass = new ScheduleClass();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(ScheduleClassType::class, $scheduleClass,array('entity_manager'=>$em));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $resp=$scheduleService->AddScheduleDates($scheduleClass, $this->getUser());
            if(isset($resp['error'])){
                $form->addError(new FormError($resp['error']));
                return $this->render('schedule_class/new.html.twig', [
                    'schedule_class' => $scheduleClass,
                    'form' => $form->createView(),
                ]);
            }
            return $this->redirectToRoute('schedule_class_index');
        }

        return $this->render('schedule_class/new.html.twig', [
            'schedule_class' => $scheduleClass,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="schedule_class_show", methods={"GET"})
     */
    public function show(ScheduleClass $scheduleClass): Response
    {
        return $this->render('schedule_class/show.html.twig', [
            'schedule_class' => $scheduleClass,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="schedule_class_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ScheduleClass $scheduleClass, ScheduleService $scheduleService): Response
    {
        $form = $this->createForm(ScheduleClassType::class, $scheduleClass);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $resp=$scheduleService->EditScheduleDates($scheduleClass, $this->getUser());
            if(isset($resp['error'])){
                $form->addError(new FormError($resp['error']));
                return $this->render('schedule_class/new.html.twig', [
                    'schedule_class' => $scheduleClass,
                    'form' => $form->createView(),
                ]);
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('schedule_class_index');
        }

        return $this->render('schedule_class/edit.html.twig', [
            'schedule_class' => $scheduleClass,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="schedule_class_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ScheduleClass $scheduleClass): Response
    {
        if ($this->isCsrfTokenValid('delete'.$scheduleClass->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($scheduleClass);
            $entityManager->flush();
        }

        return $this->redirectToRoute('schedule_class_index');
    }
}
