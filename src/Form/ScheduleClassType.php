<?php

namespace App\Form;

use App\Entity\Classes;
use App\Entity\ScheduleClass;
use App\Entity\Teacher;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ScheduleClassType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $em = $options['entity_manager'];
        $builder->getData()->getTeacher();
        $builder
            ->add('startTime', TimeType::class, [
                // renders it as a single text box
                'widget' => 'single_text',
            ])
            ->add('endTime', TimeType::class, [
                // renders it as a single text box
                'widget' => 'single_text',
            ])
            ->add('weekday', ChoiceType::class, [
                'choices' => [
                    'Sunday' => 'Sunday',
                    'Monday' => 'Monday',
                    'Tuesday' => 'Tuesday',
                    'Wednesday' => 'Wednesday',
                    'Thursday' => 'Thursday',
                    'Friday' => 'Friday',
                    'Saturday' => 'Saturday',
                ],
            ])
            ->add('class', EntityType::class, [
                'class' => Classes::class,
                'choice_label' => function ($classes) {
                    return $classes->getName();
                },
                'choice_value' => "id"
            ])
            ->add('teacher', null, ["label"=>"Teacher"]);

        $formModifier = function (FormInterface $form, $class = 1) {
            $form->add('teacher', EntityType::class, array(
                'class' => Teacher::class,
                'query_builder' => function (EntityRepository $repo) use ($class) {

                    return $repo->createQueryBuilder('t')
                        ->where('t.class=:class')
                        ->setParameter('class', $class);
                },
                'choice_label' => 'firstName',
                'choice_value' => 'id',
                'expanded' => false,
                'multiple' => false,
                'label' => 'Teacher',
            ));
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                $data = $event->getData();
                $class = $data->getTeacher();

                if (!$class) {
                    $class = 1;
                }
                $formModifier($event->getForm(), $class);
            }
        );
        $builder->get('class')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier, $em) {
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $class = $event->getForm()->getData();

                // since we've added the listener to the child, we'll have to pass on
                // the parent to the callback functions!
                $formModifier($event->getForm()->getParent(), $class);
            }
        );
    }

    public function getName()
    {
        return 'ScheduleClass';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ScheduleClass::class,
        ]);
        $resolver->setRequired('entity_manager');
    }
}
