<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210411163259 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE classes (Id INT AUTO_INCREMENT NOT NULL, Name VARCHAR(255) NOT NULL, PRIMARY KEY(Id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE schedule_class (teacher_id INT DEFAULT NULL, class_id INT DEFAULT NULL, created_by INT DEFAULT NULL, Id INT AUTO_INCREMENT NOT NULL, start_time TIME DEFAULT NULL, end_time TIME DEFAULT NULL, weekday VARCHAR(10) DEFAULT NULL, is_repeatable INT DEFAULT NULL, created_date DATETIME DEFAULT CURRENT_TIMESTAMP, updated_date DATETIME DEFAULT NULL, INDEX created_by (created_by), INDEX class_id (class_id), INDEX schedule_class_ibfk_1 (teacher_id), PRIMARY KEY(Id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE teacher (class_id INT DEFAULT NULL, Id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, INDEX class_id (class_id), PRIMARY KEY(Id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, display_name VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE schedule_class ADD CONSTRAINT FK_1112D9C341807E1D FOREIGN KEY (teacher_id) REFERENCES teacher (Id)');
        $this->addSql('ALTER TABLE schedule_class ADD CONSTRAINT FK_1112D9C3EA000B10 FOREIGN KEY (class_id) REFERENCES classes (Id)');
        $this->addSql('ALTER TABLE schedule_class ADD CONSTRAINT FK_1112D9C3DE12AB56 FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE teacher ADD CONSTRAINT FK_B0F6A6D5EA000B10 FOREIGN KEY (class_id) REFERENCES classes (Id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE schedule_class DROP FOREIGN KEY FK_1112D9C3EA000B10');
        $this->addSql('ALTER TABLE teacher DROP FOREIGN KEY FK_B0F6A6D5EA000B10');
        $this->addSql('ALTER TABLE schedule_class DROP FOREIGN KEY FK_1112D9C341807E1D');
        $this->addSql('ALTER TABLE schedule_class DROP FOREIGN KEY FK_1112D9C3DE12AB56');
        $this->addSql('DROP TABLE classes');
        $this->addSql('DROP TABLE schedule_class');
        $this->addSql('DROP TABLE teacher');
        $this->addSql('DROP TABLE user');
    }
}
