### Class Scheduler test project ###

### Before you start ###
* make sure you have installed node js in your machine (https://nodejs.org/en/)
* make sure you have installed yarn package manager in your machine (https://classic.yarnpkg.com/en/docs/install/#windows-stable)
* make sure you have installed composer in your machine (https://getcomposer.org/download/)

### How do I get set up? ###

* change database credentials in .env file in root dir
* run the command in command line  "composer install" 
* run "php bin/console doctrine:migrations:migrate" to create database tables
* run "npm install"
* run "yarn install"
* run "yarn encore dev" to create frontend builds
* to start server run "symfony server:start"

